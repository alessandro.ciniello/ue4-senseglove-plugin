// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/CapsuleComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "SenseGloveHaptics.h"
#include "SenseGloveCollider.generated.h"


//UCLASS(ShowCategories = (ComponentReplication), ClassGroup = SenseGlove, meta = (BlueprintSpawnableComponent), BluePrintable)
UCLASS(ShowCategories = (SenseGlove), ClassGroup = SenseGlove, meta = (BlueprintSpawnableComponent), BluePrintable)
class SENSEGLOVE_API USenseGloveCollider : public UCapsuleComponent
{
	GENERATED_BODY()

public:
	USenseGloveCollider();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SenseGlove")
		TArray<TEnumAsByte<SenseGloveHapticChannel>> assignedActuators;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SenseGlove")
		FName assignedBone;

	UFUNCTION(BlueprintCallable, Category = "SenseGlove")
	void BindToActuator(TEnumAsByte<SenseGloveHapticChannel> actuator);

	UFUNCTION(BlueprintCallable, Category = "SenseGlove")
	void BindToSkelet(USkeletalMeshComponent *meshContainer);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float staticHitDuration = .2f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int hitIntensity = 80;
	
private:

};
