/* robin@senseglove.com */

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/CapsuleComponent.h"
#include "GenericPlatform/IInputInterface.h"
#include "SenseGloveHaptics.h"

#include "../Thirdparty/SenseGlove/incl/SenseGlove.h"
#include "../Thirdparty/SenseGlove/incl/HapticGlove.h"
#include "../Thirdparty/SenseGlove/incl/DeviceList.h"

#include "SenseGloveController.generated.h"


UENUM(BlueprintType)
enum class GloveType : uint8
{
	RightGlove = 0,
	LeftGlove = 1
};

DECLARE_DELEGATE(FReleaseObjectDelegate);

UCLASS(Blueprintable, HideCategories = (Default, Rendering, Replication, LOD, Cooking))
class ASenseGloveController : public AActor
{
	GENERATED_BODY()

public:
	ASenseGloveController();

	std::shared_ptr<SGCore::HapticGlove> controllerHandleNew;
	
	TSubclassOf<UAnimInstance> gloveAnimation;

	class USkeletalMesh* gloveMesh;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SenseGlove")
		TEnumAsByte<GloveType> gloveType;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Instanced, Category = "SenseGlove")
		class USkeletalMeshComponent* meshContainer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SenseGlove")
		class USenseGloveCollider* thumbCapsule;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "SenseGlove")
		class USenseGloveCollider* indexCapsule;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "SenseGlove")
		class USenseGloveCollider* middleCapsule;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "SenseGlove")
		class USenseGloveCollider* ringCapsule;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "SenseGlove")
		class UBoxComponent* thumbGrabArea;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "SenseGlove")
		class UBoxComponent* indexGrabArea;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SenseGlove")
		bool bDefaultGrabbing = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SenseGlove")
		bool bDefaultPhysics = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SenseGlove")
		bool bEnableDebug = true;

	bool bNeedsRelease = false;
	bool bGrabbingGesture = false;

	virtual void Tick(float DeltaTime) override;

	virtual void PostInitializeComponents() override;

	bool sendBuzzRaw(int thumb, int index, int middle, int ring, int pinky, int wrist);

	bool sendForceFeedbackRaw(int thumb, int index, int middle, int ring, int pinky);

	UFUNCTION(BlueprintCallable, Category = "SenseGlove")
		bool sendThumperRaw(TEnumAsByte<ThumperWaveForm> wave);

	UFUNCTION(BlueprintCallable, Category = "SenseGlove")
		bool activateVibroTactileFeedback(TEnumAsByte<SenseGloveHapticChannel> Actuator, float Duration, int Intensity, bool isLooping);

	UFUNCTION(BlueprintCallable, Category = "SenseGlove")
		bool activateForceFeedback(TEnumAsByte<SenseGloveHapticChannel> Actuator, float Duration, int Intensity, bool isLooping);
	
	UFUNCTION(BlueprintCallable, Category = "SenseGlove")
		bool startCalibration();
	
	UFUNCTION(BlueprintCallable, Category = "SenseGlove")
		bool stopCalibration();

	UFUNCTION()
		void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	UFUNCTION()
		void OnOverlapEnter(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void OnOverlapExit(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
#if WITH_EDITOR
	virtual void PostEditChangeChainProperty(struct FPropertyChangedChainEvent& PropertyChangedEvent) override;
#endif

	FReleaseObjectDelegate releaseObject;
    	
	SGCore::HandProfile handProfile;
    	bool rightHanded = true;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

private:
	
	int maxVibroChannels = 6;
	int maxFFChannels = 5;

	FSenseGloveHapticActuator ffActuators[SenseGloveHapticChannel::TOTAL];
	FSenseGloveHapticActuator vibroActuators[SenseGloveHapticChannel::TOTAL];

	void initializeGrabbing();
	void initializePhysics();
	void initializeMeshContainer();
	void deInitializePhysics();
	void deInitializeGrabbing();

	void releaseObjects();

	AActor* tmpThumbObject;
	AActor* tmpIndexObject;
	AActor* grabObject;

	UPrimitiveComponent* tmpThumbComp;
	UPrimitiveComponent* tmpIndexComp;
	UPrimitiveComponent* grabComp;

	bool isGrabbing = false;

	/* HACK:  not let force feedback channels 
	overwrite vibro haptics */
	int ff[5] = { 0 };
	int buzz[6] = { 0 };

};


