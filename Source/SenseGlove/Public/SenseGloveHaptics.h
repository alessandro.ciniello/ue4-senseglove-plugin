// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
#pragma once

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "UObject/Object.h"
#include "Curves/CurveFloat.h"
#include "SenseGloveHaptics.generated.h"

class USenseGloveHaptics;

UENUM(BlueprintType)
enum class SenseGloveHapticChannel : uint8
{
	THUMB = 0,
	INDEX,
	MIDDLE,
	RING,
	PINKY,
	WRIST,
	TOTAL
};

UENUM(BlueprintType)
enum class ThumperWaveForm : uint8
{
	Blank = 0,
	Impact_Thump_100 = 1,
	Impact_Thump_30 = 3,
	Impact_Thump_10 = 6,
	Object_Grasp_100 = 7,
	Object_Grasp_60 = 8,
	Object_Grasp_30 = 9,
	Button_Double_100 = 10,
	Button_Double_60 = 11,
	Cue_Game_Over = 118,
	Turn_Off = 124,
	None = 126
};

USTRUCT()
struct FSenseGloveHapticActuator
{
	GENERATED_BODY()

		FSenseGloveHapticActuator()
		: channel(0)
		, currentValue(0)
		, duration(.0f)
		, isLooping(0)
		, bNeedsUpdate(false)
	{}

	FSenseGloveHapticActuator(TEnumAsByte<SenseGloveHapticChannel> ch, int cur, float dur, bool loop, bool nu)
		: channel(ch)
		, currentValue(cur)
		, duration(dur)
		, isLooping(loop)
		, bNeedsUpdate(nu)
	{}

	TEnumAsByte<SenseGloveHapticChannel> channel;

	int currentValue;

	float duration;

	bool bNeedsUpdate;

	bool isLooping;

	bool Update(float DeltaTime);
};


/**
 * A predefined force-feedback effect to be played on a controller
 */
UCLASS(BlueprintType, EditInlineNew)
class USenseGloveHaptics : public UObject
{
	GENERATED_UCLASS_BODY()

		//UPROPERTY(EditAnywhere, Category = "ForceFeedbackEffect")
		//TArray<FForceFeedbackChannelDetails> ChannelDetails;

	/** Duration of force feedback pattern in seconds. */
		UPROPERTY(Category = Info, AssetRegistrySearchable, VisibleAnywhere)
		float Duration;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float Duration2;

#if WITH_EDITOR
	virtual void PostEditChangeChainProperty(struct FPropertyChangedChainEvent& PropertyChangedEvent) override;
#endif

	float GetDuration();

	void GetValues() const;
};

