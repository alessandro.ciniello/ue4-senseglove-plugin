// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "SenseGloveForceFeedback.h"
#include "Misc/App.h"

USenseGloveForceFeedback::USenseGloveForceFeedback(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	// Make sure that by default the force feedback effect has an entry
	//FForceFeedbackChannelDetails ChannelDetail;
	//ChannelDetails.Add(ChannelDetail);
}

#if WITH_EDITOR
void USenseGloveForceFeedback::PostEditChangeChainProperty(struct FPropertyChangedChainEvent& PropertyChangedEvent)
{
	// After any edit (really we only care about the curve, but easier this way) update the cached duration value
	//GetDuration();
}

#endif

float USenseGloveForceFeedback::GetDuration()
{
	return 1.0f;
}

void USenseGloveForceFeedback::GetValues() const
{

}

/* returns true if actuator needs to be fired (updated) 
 false otherwise */

bool FSenseGloveForceFeedBackActuator::Update(const float DeltaTime)
{
	if (this->duration <= 0 || this->isLooping)
		return false;

	this->duration -= DeltaTime;
	if (this->duration <= 0) {
		this->currentValue = 0;
	}

	return true;

}
