# UE4 Senseglove Plugin

SenseGlove plugin for Unreal Engine 4. 
Note: If you are starting a project from scratch, it's more convenient to use the demo scene: https://gitlab.com/senseglove/ue4-senseglove-demo-scene

## Prerequisites

1. Unreal Engine v4.26 and upwards
2. SenseCom.exe and SGCoreCpp.dll. Available here: https://github.com/Adjuvo/SenseGlove-API
3. A **C++** Unreal Project. A blueprints-only project will have to be converted to a C++ project (Depending on your project setup this might be possible by right clicking .uproject file in your project top directory > Generate Visual Studio Files) 

## Install and configure

1. Copy this repository into either the 'Global' plugin directory or 'Local' Plugin directory of your project. 
If you don't have any Plugins directory in your project, you should create a Plugins directory and put this Plugin in a subdirectory:
Directory structure should be similar to: *My UE4 Project > Plugins > UE4_SenseGlove*
2. Copy SGCoreCore.dll into the 'Binaries/Win64' directory of your project.

## Getting basic Hand Tracking and Default Interactions

1. Make sure SenseCom.exe is running and a SenseGlove or Nova is connected.
2. Add a new Blueprint *SenseGloveController* class to your Project.
3. Open this blueprint and select Left or Right Glove under Details > SenseGlove.
![Screenshot](https://gitlab.com/senseglove/ue4-senseglove-plugin/-/raw/master/Resources/details.png "details")
4. Link a compatible Skeletal Mesh (.fbx are available under Plugin/Resources) to the Skeletal Mesh Component. (Skeletal Mesh Component > Mesh > Skeletal Mesh)
5. Set 'SenseGloveAnimInstance' as Animation Class to this Skeletal Mesh. (Skeletal Mesh Component > Animation > Anim Class > SenseGloveAnimInstance)
![Screenshot](https://gitlab.com/senseglove/ue4-senseglove-plugin/-/raw/master/Resources/meshcontainer.png "mesh animation")
![Screenshot](https://gitlab.com/senseglove/ue4-senseglove-plugin/-/raw/master/Resources/meshcontainer2.png "mesh animation2")
6. (Optional) Press *Simulate* in your blueprint to test your settings.
7. Drag your SenseGloveController blueprint to your scene, this blueprint will have hand tracking, physics and grabbing enabled.
8. (Required for VR 3D tracking, manual step). Link your SenseGloveController blueprint to a HTC/Oculus tracker (see Demo for an example)

## Creating custom events
The SensegloveController class has a variety of built-in commands that allows you to create custom commands.

1. Open any Blueprint class and navigate to the Event Graph
2. Right click and search for SenseGlove. This will show a list of commands that are available. 
![Screenshot](https://gitlab.com/senseglove/ue4-senseglove-plugin/-/raw/master/Resources/custom-interaction.gif "haptic commands")
3. You can bind these action to collision boxes or other scene components.
